#include <iostream>

using namespace std;

void selectionSort (int *, int n);

int main () {
	int n, i;

	cout << "Selection Sort!" << endl;

	cout << "Insira o tamanho: ";
	cin >> n;

	int vet [n];

	cout << "Insira os elementos: " << endl;
	for (i = 0; i < n; i++) {
		cin >> vet[i];
	}

	cout << "Vetor nao organizado: ";
	for (i = 0; i < n; i++) {
		cout << vet [i];
	}

	cout << endl;

	selectionSort (vet, n);

	return 0;
}

void selectionSort (int vet [], int n) {
	int i, j, aux, min;

	for (i = 0; i < n - 1; i++) {
		min = i;

		for (j = i + 1; j < n; j++) {
			if(vet[j] < vet[min]) {
				min = j;
			}
		}

		if (vet [min] < vet [i]) {
			aux = vet [i];
			vet [i] = vet [min];
			vet [min] = aux;
		}
	}

	cout << "Vetor organizado: ";
	for (i = 0; i < n; i++) {
		cout << vet [i];
	}
}