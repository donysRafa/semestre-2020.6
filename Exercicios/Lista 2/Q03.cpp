#include <iostream>

using namespace std;

void insertionSort (int *, int n);

int main () {
	int n, i;

	cout << "Insertion Sort!" << endl;

	cout << "Insira o tamanho: ";
	cin >> n;

	int vet [n];

	cout << "Insira os elementos: " << endl;
	for (i = 0; i < n; i++) {
		cin >> vet[i];
	}

	cout << "Vetor nao organizado: ";
	for (i = 0; i < n; i++) {
		cout << vet [i];
	}

	cout << endl;

	insertionSort (vet, n);

	return 0;
}

void insertionSort (int vet [], int n) {
	int aux, i, j;

	for (i = 0; i < n -1; i++) {
		if (vet [i+1] < vet [i]) {
			for (j = i + 1; j > 0; j--) {
				if (vet [j] < vet [j-1]) {
					aux = vet [j-1];
					vet [j-1] = vet [j];
					vet [j] = aux;
				}
			}
		}
	}

	cout << "vetor organizado: ";

	for (i = 0; i < n; i++) {
		cout << vet [i];
	}
}