#include <iostream>

using namespace std;

void bubbleSort (int* , int n);

int main () {
	int n, i;

	cout << "Bubble Sort!" << endl;

	cout << "Insira o tamanho: ";
	cin >> n;

	int vet [n];

	cout << "Insira os elementos: " << endl;
	for (i = 0; i < n; i++) {
		cin >> vet[i];
	}

	cout << "Vetor nao organizado: ";
	for (i = 0; i < n; i++) {
		cout << vet [i];
	}

	cout << endl;

	bubbleSort (vet, n);

	return 0;
}

void bubbleSort (int vet [], int n) {
	int aux, i, j;
	int count = n;

	while ((n - 1) > 0){
		for (i = 0; i < n; i++) {
			if (vet [i] > vet [i+1]) {
				aux = vet [i];
				vet [i] = vet [i+1];
				vet [i+1] = aux;
			}
		}

		n--;
	}

	cout << "vetor organizado: ";

	for (i = 0; i < count; i++) {
		cout << vet [i];
	}
}