#include <iostream>

using namespace std;

int busca (int qtd_num, int seq [], int num) {

	for (int i = 0; i < qtd_num; i++){
		if(seq [i] == num) {
			return i;
		}
	}
	
	return -1;
}

int main () {

	int qtd_num, num, seq [];

	cout << "Digite a quatidade de numeros da sequencia: " << endl;
	cin >> qtd_num;

	cout << "Digite os numeros que deseja para a sequencia: " << endl;

	for (int i = 0; i < qtd_num; i ++){
		cin >> seq [i];
	}

	cout << "Digite o número que desejas: " << endl;
	cin >> num;

	busca (qtd_num, seq, num);

	return 0;
}